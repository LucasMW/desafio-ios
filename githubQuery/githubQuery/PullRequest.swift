//
//  PullRequest.swift
//  githubQuery
//
//  Created by Lucas Menezes on 04/11/16.
//  Copyright © 2016 MWAPPTECH. All rights reserved.
//

import UIKit

class PullRequest  {
    let author : String!
    let description: String!
    let date : String
    let image : UIImage!
    let title : String!
    
    init(author : String, description: String!, date: String, image: UIImage!, title : String)
    {
        self.author = author
        self.description = description
        self.date = date
        self.image = image
        self.title = title
    }
}
