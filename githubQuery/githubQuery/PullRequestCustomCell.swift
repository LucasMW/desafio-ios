//
//  PullRequestCustomCell.swift
//  githubQuery
//
//  Created by Lucas Menezes on 04/11/16.
//  Copyright © 2016 MWAPPTECH. All rights reserved.
//

import UIKit

class PullRequestCustomCell: UITableViewCell {

    @IBOutlet weak var authorImage: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var commitTitle: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        print("jka")
        // Initialization code
    }

    func setData(pr : PullRequest) {
        //print("sd")
        authorImage.image = pr.image
        dateLabel.text = String(describing: pr.date)
        bodyLabel.text = pr.description
        commitTitle.text = pr.title
        authorLabel.text = pr.author
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //print("Hi")
        // Configure the view for the selected state
    }

}
