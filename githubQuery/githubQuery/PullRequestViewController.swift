//
//  PullRequestViewController.swift
//  githubQuery
//
//  Created by Lucas Menezes on 04/11/16.
//  Copyright © 2016 MWAPPTECH. All rights reserved.
//

import UIKit

class PullRequestViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var pullRequests : [PullRequest] = [PullRequest]()
    private var repoName : String!
    @IBOutlet weak var repoNameLabel: UILabel!
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
//     UIApplication.shared.keyWindow?.rootViewController = vcToReturn
         self.removeFromParentViewController()
    }
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    var vcToReturn : UIViewController!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.register(UINib(nibName: "CustomPullRequestsCell", bundle: nil), forCellReuseIdentifier: "pullCustomCell")
        self.repoNameLabel.text = repoName
    }
    
    func setReturnVc( vc : UIViewController!) {
        self.vcToReturn = vc
    }
    func performQuery(urlStr : String) {
        let url = URL(string: urlStr)
        URLSession.shared.dataTask(with: url!) { (data,response,error) in
            if error != nil {
                //print("ERROR \(error)")
            } else {
                do {
                    let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                    
                    if let items  = json as? [[String : Any]] {
                    print("items")
                    for item in items {
                        
                        let user = item["user"] as! [String : Any]
                        let imgUrl = URL(string: user["avatar_url"] as! String)
                        let data = NSData(contentsOf: imgUrl!)
                        let img = UIImage(data: data as! Data)!
                        let df = DateFormatter()
                        let dateStr = item["updated_at"] as! String
                        
                        let pr = PullRequest(author: user["login"] as! String,
                            description: item["body"] as! String,
                            date: dateStr,
                            image: img,
                            title: item["title"] as! String)
                        self.pullRequests.append(pr)
                        
                    }
                        self.didFinishQuery()
                    }
                } catch let error as NSError {
                    //print("ERROR \(error)")
                }
            
            }
            }.resume()
        
    }
    func loadPullRequests(creator : String, repo : String) {
        self.repoName = repo
        let str = "https://api.github.com/repos/\(creator)/\(repo)/pulls"
        print(str)
        performQuery(urlStr: str)
    }
    func didFinishQuery() {
        print("Query finished")
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let pr = self.pullRequests[row]
        print(pr.title)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pullCustomCell") as! PullRequestCustomCell
       
        let row = indexPath.row
        let pr = self.pullRequests[row]
        cell.setData(pr: pr)
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
