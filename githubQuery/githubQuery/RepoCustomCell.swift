//
//  RepoCustomCell.swift
//  githubQuery
//
//  Created by Lucas Menezes on 04/11/16.
//  Copyright © 2016 MWAPPTECH. All rights reserved.
//

import UIKit

class RepoCustomCell: UITableViewCell {

    @IBOutlet weak var authorImage: UIImageView!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var forks: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    var gitRepo : GitRepo!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    func setRepo(repo: GitRepo) {
        print("sfs")
        self.gitRepo = repo
        self.name.text = gitRepo.name
        self.starsLabel.text = "🌟\(gitRepo.starsNumber!)"
        self.forks.text = "🍴\(gitRepo.forkNumber!)"
        self.authorImage.image = gitRepo.authorImage
        self.descriptionLabel.text = gitRepo.description
        self.authorLabel.text = gitRepo.owner
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
