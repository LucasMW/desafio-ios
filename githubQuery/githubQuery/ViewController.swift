//
//  ViewController.swift
//  githubQuery
//
//  Created by Lucas Menezes on 04/11/16.
//  Copyright © 2016 MWAPPTECH. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var repos: [GitRepo] = [GitRepo]()
    @IBOutlet weak var tableView: UITableView!
    var currentPageToLoad : Int!
    var loading: Bool!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.register(UINib(nibName: "CustomRepoCell", bundle: nil), forCellReuseIdentifier: "repoCustomCell")
        
        self.currentPageToLoad = 1
        self.loading = true
        self.performQuery(page: self.currentPageToLoad)
        self.currentPageToLoad = self.currentPageToLoad + 1
        
    }
    func didFinishQuery() {
        
        print("Query finished")
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.loading = false
        }
        
        //self.tableView.setNeedsDisplay()
        //print(repos)
    }
    func performQuery(page : Int) {
        let urlStr = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
        print(urlStr)
        let url = URL(string: urlStr)
        URLSession.shared.dataTask(with: url!) { (data,response,error) in
            if error != nil {
                print("ERROR \(error)")
            } else {
                do {
                    let parsedData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: Any]
                    let items = parsedData["items"] as! [[String : Any]]
                
                    for item in parsedData["items"] as! [[String : Any]]{
                        
                        let owner = item["owner"] as! [String : Any]
                        let imgUrl = URL(string: owner["avatar_url"] as! String)
                        let data = NSData(contentsOf: imgUrl!)
                        let img = UIImage(data: data as! Data)!
                        let description = item["description"] != nil ? item["description"] as! String : "NULL"
                        let repo = GitRepo(name: item["name"] as! String,
                                           description: description,
                                           authorImage: img,
                                           starsNumber: item["stargazers_count"] as! Int,
                                           forkNumber: item["forks"] as! Int,
                                           owner: owner["login"] as! String)
                        self.repos.append(repo)
                    }
                } catch let error as NSError {
                    print("ERROR \(error)")
                }
                self.didFinishQuery()
            }
            }.resume()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let repo = self.repos[row]
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PullRequestViewController") as! PullRequestViewController
        vc.setReturnVc(vc: self)
        vc.loadPullRequests(creator: repo.owner, repo: repo.name)
        self.present(vc, animated: true, completion: nil)
        print(repo.name)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "repoCustomCell") as! RepoCustomCell
        let row = indexPath.row
        let repo = self.repos[row]
        cell.setRepo(repo: repo)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.repos.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset;
        let bounds = scrollView.bounds;
        let size = scrollView.contentSize;
        let inset = scrollView.contentInset;
        let y = offset.y + bounds.size.height - inset.bottom;
        let h = size.height;
        
        let reload_distance : CGFloat = 50;
        if(y > h + reload_distance) {
            print("should reload")
            if(self.loading == false) {
                print("loading")
                self.loading = true
                performQuery(page: self.currentPageToLoad)
                self.currentPageToLoad = self.currentPageToLoad + 1
            }
            
            
        }
    }
    
    

}

