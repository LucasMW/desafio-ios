//
//  GitRepo.swift
//  githubQuery
//
//  Created by Lucas Menezes on 04/11/16.
//  Copyright © 2016 MWAPPTECH. All rights reserved.
//

import UIKit

class GitRepo {
    let name : String!
    let description : String!
    let authorImage : UIImage!
    let starsNumber : Int!
    let forkNumber : Int!
    let owner : String!
    
    init(name : String,description: String, authorImage : UIImage, starsNumber : Int, forkNumber : Int, owner : String!) {
        self.name = name
        self.description = description
        self.authorImage = authorImage
        self.starsNumber = starsNumber
        self.forkNumber = forkNumber
        self.owner = owner
    }

}
